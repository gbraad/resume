Gerard Braad
============

  * [Resume](resume.md)
  * [Recommendations](recommendations.md)
  * [Portfolio](//gbraad.nl/#portfolio)


| [!["Gerard Braad"](http://gravatar.com/avatar/e466994eea3c2a1672564e45aca844d0.png?s=60)](http://gbraad.nl "Gerard Braad <me@gbraad.nl>") |
|---|
| [@gbraad](https://gbraad.nl/social) |
